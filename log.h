
#include <stdio.h>
#include <stdlib.h>

#ifdef CREATE_LOG
extern FILE* log_file;
#endif

#ifdef CREATE_LOG
#define LOG_LINE(...) fprintf(log_file, __VA_ARGS__); fflush(log_file);
#else
#define LOG_LINE(...) ;
#endif
