
#include "pst.h" 

int get_value_pst_64(const pst_64 pst, const int x, const int y, const int flip)
{
  if (flip)
  {
    return pst[8 * y + x];
  }
  else
  {
    return pst[8 * (7 - y) + x];
  }
}

int get_value_pst_80(const pst_80 pst, const int x, const int y, const int flip)
{
  if (flip)
  {
    return pst[10 * y + x];
  }
  else
  {
    return pst[10 * (7 - y) + x];
  }
}

int get_value_pst(const int board_10x8, const char piece, const int x, const int y)
{
  int result;
  
  if (board_10x8)
  {
    switch(piece)
    {
      case 'A': result = get_value_pst_80(pst_80_archbishop, x, y, 0); break;
      case 'B': result = get_value_pst_80(pst_80_bishop,     x, y, 0); break;
      case 'C': result = get_value_pst_80(pst_80_chancellor, x, y, 0); break;
      case 'K': result = get_value_pst_80(pst_80_king,       x, y, 0); break;
      case 'N': result = get_value_pst_80(pst_80_knight,     x, y, 0); break;
      case 'P': result = get_value_pst_80(pst_80_pawn,       x, y, 0); break;
      case 'Q': result = get_value_pst_80(pst_80_queen,      x, y, 0); break;
      case 'R': result = get_value_pst_80(pst_80_rook,       x, y, 0); break;
      case 'a': result = get_value_pst_80(pst_80_archbishop, x, y, 1); break;
      case 'b': result = get_value_pst_80(pst_80_bishop,     x, y, 1); break;
      case 'c': result = get_value_pst_80(pst_80_chancellor, x, y, 1); break;
      case 'k': result = get_value_pst_80(pst_80_king,       x, y, 1); break;
      case 'n': result = get_value_pst_80(pst_80_knight,     x, y, 1); break;
      case 'p': result = get_value_pst_80(pst_80_pawn,       x, y, 1); break;
      case 'q': result = get_value_pst_80(pst_80_queen,      x, y, 1); break;
      case 'r': result = get_value_pst_80(pst_80_rook,       x, y, 1); break;
      default : result =0;
    }
  }
  else
  {
    switch(piece)
    {
      case 'B': result = get_value_pst_64(pst_64_bishop,     x, y, 0); break;
      case 'K': result = get_value_pst_64(pst_64_king,       x, y, 0); break;
      case 'N': result = get_value_pst_64(pst_64_knight,     x, y, 0); break;
      case 'P': result = get_value_pst_64(pst_64_pawn,       x, y, 0); break;
      case 'Q': result = get_value_pst_64(pst_64_queen,      x, y, 0); break;
      case 'R': result = get_value_pst_64(pst_64_rook,       x, y, 0); break;
      case 'b': result = get_value_pst_64(pst_64_bishop,     x, y, 1); break;
      case 'k': result = get_value_pst_64(pst_64_king,       x, y, 1); break;
      case 'n': result = get_value_pst_64(pst_64_knight,     x, y, 1); break;
      case 'p': result = get_value_pst_64(pst_64_pawn,       x, y, 1); break;
      case 'q': result = get_value_pst_64(pst_64_queen,      x, y, 1); break;
      case 'r': result = get_value_pst_64(pst_64_rook,       x, y, 1); break;
      default : result =0;
    }
  }
  
  return result;
}

/* https://www.chessprogramming.org/Simplified_Evaluation_Function */

const pst_64 pst_64_pawn =
{
   0, 0, 0, 0, 0, 0, 0, 0,
   9, 9, 9, 9, 9, 9, 9, 9,
   2, 2, 4, 6, 6, 4, 2, 2,
   1, 1, 2, 5, 5, 2, 1, 1,
   0, 0, 0, 4, 4, 0, 0, 0,
   1,-1,-2, 0, 0,-2,-1, 1,
   1, 2, 2,-4,-4, 2, 2, 1,
   0, 0, 0, 0, 0, 0, 0, 0
};

const pst_64 pst_64_knight =
{
  -9,-8,-6,-6,-6,-6,-8,-9,
  -8,-4, 0, 0, 0, 0,-4,-8,
  -6, 0, 2, 3, 3, 2, 0,-6,
  -6, 1, 3, 4, 4, 3, 1,-6,
  -6, 0, 3, 4, 4, 3, 0,-6,
  -6, 1, 2, 3, 3, 2, 1,-6,
  -8,-4, 0, 1, 1, 0,-4,-8,
  -9,-8,-6,-6,-6,-6,-8,-9
};

const pst_64 pst_64_bishop =
{
  -4,-2,-2,-2,-2,-2,-2,-4,
  -2, 0, 0, 0, 0, 0, 0,-2,
  -2, 0, 1, 2, 2, 1, 0,-2,
  -2, 1, 1, 2, 2, 1, 1,-2,
  -2, 0, 2, 2, 2, 2, 0,-2,
  -2, 2, 2, 2, 2, 2, 2,-2,
  -2, 1, 0, 0, 0, 0, 1,-2,
  -4,-2,-2,-2,-2,-2,-2,-4
};

const pst_64 pst_64_rook =
{
   0, 0, 0, 0, 0, 0, 0, 0,
   1, 2, 2, 2, 2, 2, 2, 1,
  -1, 0, 0, 0, 0, 0, 0,-1,
  -1, 0, 0, 0, 0, 0, 0,-1,
  -1, 0, 0, 0, 0, 0, 0,-1,
  -1, 0, 0, 0, 0, 0, 0,-1,
  -1, 0, 0, 0, 0, 0, 0,-1,
   0, 0, 0, 1, 1, 0, 0, 0
};

const pst_64 pst_64_queen =
{
  -4,-2,-2,-1,-1,-2,-2,-4,
  -2, 0, 0, 0, 0, 0, 0,-2,
  -2, 0, 1, 1, 1, 1, 0,-2,
  -1, 0, 1, 1, 1, 1, 0,-1,
   0, 0, 1, 1, 1, 1, 0,-1,
  -2, 1, 1, 1, 1, 1, 0,-2,
  -2, 0, 1, 0, 0, 0, 0,-2,
  -4,-2,-2,-1,-1,-2,-2,-4
};

const pst_64 pst_64_king =
{
  -6,-8,-8,-9,-9,-8,-8,-6,
  -6,-8,-8,-9,-9,-8,-8,-6,
  -6,-8,-8,-9,-9,-8,-8,-6,
  -6,-8,-8,-9,-9,-8,-8,-6,
  -4,-6,-6,-8,-8,-6,-6,-4,
  -2,-4,-4,-4,-4,-4,-4,-2,
   4, 4, 0, 0, 0, 0, 4, 4,
   4, 6, 2, 0, 0, 2, 6, 4
};

const pst_80 pst_80_pawn =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_knight =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_bishop =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_archbishop =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_rook =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_chancellor =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_queen =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const pst_80 pst_80_king =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
