
#if defined(__LP64__)
#define TARGET_CPU "x86_64"
#elif defined(__i386__)
#define TARGET_CPU "i386"
#else
#define TARGET_CPU "TARGETCPU"
#endif

#if defined(__linux__)
#define TARGET_OS "Linux"
#elif defined(_WIN32)
#define TARGET_OS "Windows"
#else
#define TARGET_OS "TARGETOS"
#endif

#if defined(__clang__)
#define COMPILER "Clang"
#elif defined(__MINGW32__)
#define COMPILER "MinGW"
#elif defined(__MINGW64__)
#define COMPILER "MinGW"
#elif defined(__TINYC__)
#define COMPILER "TCC"
#elif defined(__GNUC__)
#define COMPILER "GCC"
#else
#define COMPILER "COMPILER"
#endif

#define BUILD_DATE __DATE__ " " __TIME__

#if defined(__VERSION__)
#define COMPILER_VERSION __VERSION__
#else
#define COMPILER_VERSION "COMPILER_VERSION"
#endif
