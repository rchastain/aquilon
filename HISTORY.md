# History

## Version 0.0.1

- First working version of the engine
- Plays random moves

## Version 0.0.2

- Accept castling in traditional notation

## Version 0.0.3

- Send castling in traditional notation (variants normal and capablanca)

## Version 0.0.3.1

- Send castling in traditional notation even when variant is not explicitely defined

## Version 0.0.4

- Basic best move function

## Version 0.0.5

- Send castling in traditional notation even when variant is not explicitely defined
- Small improvements (?) in best move function
