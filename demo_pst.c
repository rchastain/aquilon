
#include <stdio.h>

#include "pst.h"

void print_values_64(const pst_64 pst, const int black)
{
  int x, y;
  
  for (y = 9; y >= -5; y --)
  {
    for (x = -2; x <= 11; x ++)
    {
      if (y >= 0 && y <= 7 && x >= 0 && x <= 7)
      {
        printf("%03d ",  get_value_pst_64(pst, x, y, black));
      }
      else
      {
        printf("... ");
      }
    }
    printf("\n");
  }
}

void print_values_80(const pst_80 pst, const int black)
{
  int x, y;
  
  for (y = 9; y >= -5; y --)
  {
    for (x = -2; x <= 11; x ++)
    {
      if (y >= 0 && y <= 7 && x >= 0 && x <= 9)
      {
        printf("%03d ",  get_value_pst_80(pst, x, y, black));
      }
      else
      {
        printf("... ");
      }
    }
    printf("\n");
  }
}

int main(void)
{
  print_values_64(pst_64_pawn, 0);
  printf("\n");
  
  print_values_64(pst_64_pawn, 1);
  printf("\n");
  
  print_values_80(pst_80_pawn, 0);
  printf("\n");
  
  print_values_80(pst_80_pawn, 1);
  printf("\n");
  
  return 0;
}
