
#ifndef PST_H
#define PST_H

typedef int pst_64[64];
typedef int pst_80[80];

int get_value_pst_64(const pst_64 pst, const int x, const int y, const int flip);
int get_value_pst_80(const pst_80 pst, const int x, const int y, const int flip);
int get_value_pst(const int board_10x8, const char piece, const int x, const int y);

extern const pst_64 pst_64_pawn;
extern const pst_64 pst_64_knight;
extern const pst_64 pst_64_bishop;
extern const pst_64 pst_64_archbishop;
extern const pst_64 pst_64_rook;
extern const pst_64 pst_64_chancellor;
extern const pst_64 pst_64_queen;
extern const pst_64 pst_64_king;

extern const pst_80 pst_80_pawn;
extern const pst_80 pst_80_knight;
extern const pst_80 pst_80_bishop;
extern const pst_80 pst_80_archbishop;
extern const pst_80 pst_80_rook;
extern const pst_80 pst_80_chancellor;
extern const pst_80 pst_80_queen;
extern const pst_80 pst_80_king;

#endif
