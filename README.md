# Aquilon

Chess program using a chessboard of eighty squares, and able to play Capablanca, Fischer and traditional chess.

## Protocol

*Aquilon* answers to the [CECP](http://hgm.nubati.net/CECP.html) protocol (commonly named XBoard protocol).
