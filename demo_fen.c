
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "chess.h"

#define STARTPOS_CLASSICAL "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
#define STARTPOS_CAPABLANCA "rnabqkbcnr/pppppppppp/10/10/10/10/PPPPPPPPPP/RNABQKBCNR w KQkq - 0 1"

#define CASTLING_CLASSICAL "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQkq - 0 1"

int main(int argc, char *argv[])
{
  struct t_position position;
  char fen_i[256], fen_o[256];
  const char sample[] = CASTLING_CLASSICAL;
  struct t_move moves[256];
  int movecount, i;
  char move_str[6];
  struct t_position_info info;
  
  erase_position(&position);
  
  if (argc > 1) { strcpy(fen_i, argv[1]); } else { strcpy(fen_i, sample); }
  load_position_from_fen(&position, fen_i);
  
  print_position(position);
  printf("\n");
  
  get_fen(position, fen_o, sizeof(fen_o));
  
  printf("fen_i %s\n", fen_i);
  printf("fen_o %s\n", fen_o);
  printf("\n");
  
  prettyprint_position(position);
  printf("\n");
  
  info = get_position_info(&position, 0);
  
  print_info(info);
  printf("\n");
  
  movecount = generate_legal_moves(position, info, moves, sizeof(moves) / sizeof(struct t_move), 0);
  
  for (i = 0; i < movecount; i ++)
  {
	move2str(moves[i], move_str, sizeof(move_str));
    printf("%-6s", move_str);
    if ((i + 1) % 12 == 0 || i == movecount - 1)
    {
      printf("\n");
    }
  }
  
  return 0;
}
