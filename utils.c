
#include <stdio.h>
#include <string.h>

#include "utils.h"
#include "log.h"

void sqr2xy(const int i, int* x, int* y)
{
  *x = (i - 35) / 15;
  *y = (i - 35) % 15;
}

int sqr2y(const int i)
{
  return (i - 35) % 15;
}

int sqr2x(const int i)
{
  return (i - 35) / 15;
}

int xy2sqr(const int x, const int y)
{
  return 15 * x + y + 35;
}

int str2sqr(const char str[])
{
  int x, y;
  
  if (strlen(str) == 2)
  {
    x = str[0] - 'a';
    y = str[1] - '1';
    
    return xy2sqr(x, y);
  }
  else
  {
    LOG_LINE("DEBUG cannot convert square name '%s' (%s, line %d)\n", str, __FILE__, __LINE__)
    return -1;
  }
}

void sqr2str(const int i, char* str, const size_t str_size)
{
  int x, y;
  if (str_size > 2)
  {
    sqr2xy(i, &x, &y);
    str[0] = x + 'a';
    str[1] = y + '1';
    str[2] = '\0';
  }
}

int ischessmove(const char str[])
{
  return (
   strlen(str) >= 4 &&
   str[0] >= 'a' && str[0] <= 'j' &&
   str[1] >= '1' && str[1] <= '8' &&
   str[2] >= 'a' && str[2] <= 'j' &&
   str[3] >= '1' && str[3] <= '8' &&
   (strlen(str) == 4 || strchr("nbrqac", str[4]) != NULL)
  );
}
