# Aquilon

Programme d'échecs en C utilisant un échiquier de 80 cases, et sachant jouer aux échecs traditionnels, aux échecs de Capablanca et aux échecs de Fischer.

## Protocole

*Aquilon* répond au protocole [CECP](http://hgm.nubati.net/CECP.html) (plus connu sous le nom de protocole XBoard ou WinBoard).

Le code de l'interface CECP est en grande partie emprunté au programme [NEG](https://home.hccnet.nl/h.g.muller/NEG.c).

## Format FEN

Le programme est capable de lire les formats X-FEN et S-FEN.

## Échiquier interne

L'échiquier interne est un échiquier de 210 cases. Cette idée est empruntée à Reinhard Scharnagl (auteur du format X-FEN et inventeur des échecs Capablanca aléatoires).

```
.. .. .. .. .. .. .. .. .. .. .. .. .. .. ..
.. .. .. .. .. .. .. .. .. .. .. .. .. .. ..
.. .. .. .. .. A1 A2 A3 A4 A5 A6 A7 A8 .. ..
.. .. .. .. .. B1 B2 B3 B4 B5 B6 B7 B8 .. ..
.. .. .. .. .. C1 C2 C3 C4 C5 C6 C7 C8 .. ..
.. .. .. .. .. D1 D2 D3 D4 D5 D6 D7 D8 .. ..
.. .. .. .. .. E1 E2 E3 E4 E5 E6 E7 E8 .. ..
.. .. .. .. .. F1 F2 F3 F4 F5 F6 F7 F8 .. ..
.. .. .. .. .. G1 G2 G3 G4 G5 G6 G7 G8 .. ..
.. .. .. .. .. H1 H2 H3 H4 H5 H6 H7 H8 .. ..
.. .. .. .. .. I1 I2 I3 I4 I5 I6 I7 I8 .. ..
.. .. .. .. .. J1 J2 J3 J4 J5 J6 J7 J8 .. ..
.. .. .. .. .. .. .. .. .. .. .. .. .. .. ..
.. .. .. .. .. .. .. .. .. .. .. .. .. .. ..
```

## Création d'images au format PNG

Le programme est capable de créer des images au format PNG.

![image](image.png)

Les pièces proviennent du programme SMIRF de Reinhard Scharnagl.
