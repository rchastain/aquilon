
void sqr2xy(const int i, int* x, int* y);
int sqr2y(const int i);
int sqr2x(const int i);
int xy2sqr(const int x, const int y);
int str2sqr(const char str[]);
void sqr2str(const int i, char* str, const size_t str_size);
int ischessmove(const char str[]);
