
#include "vectors.h"

const int vector_white_pawn[3] = {1, -14, 16};
const int vector_black_pawn[3] = {-1, 14, -16};
const int vector_rook[4]       = {-15, 1, 15, -1};
const int vector_knight[8]     = {-17, -31, -29, -13, 17, 31, 29, 13};
const int vector_bishop[4]     = {-16, -14, 16, 14};
