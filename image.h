
#ifdef _WIN32
#include <cairo.h>
#else
#include <cairo/cairo.h>
#endif

#define IMAGE_PATH "images/"
#define CHESSBOARD_FONT "Source Code Pro"
#define LIGHT_SQUARE_COLOR 0.53, 0.53, 0.53
#define DARK_SQUARE_COLOR  0.47, 0.47, 0.47
#define BORDER_COLOR       0.50, 0.50, 0.50
#define TEXT_COLOR         0.01, 0.01, 0.01

void create_images(void);
void free_images(void);
void draw_empty_chessboard(cairo_t* ctx);
void draw_chesspiece(cairo_t* ctx, const char piece, const int x, const int y);
