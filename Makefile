#CC=tcc
CC=gcc

demo_cecp: demo_cecp.c chess.c utils.c vectors.c
	$(CC) -Wall -Wpedantic -o aquilon $^ -g -D CREATE_LOG

demo_cecp_release: demo_cecp.c chess.c utils.c vectors.c
	$(CC) -O2 -o aquilon64 $^
	$(CC) -O2 -o aquilon32 $^ -m32

demo_cecp2: demo_cecp2.c chess.c utils.c vectors.c best.c pst.c
	$(CC) -Wall -Wpedantic -o aquilon $^ -g -D CREATE_LOG

demo_cecp2_release: demo_cecp2.c chess.c utils.c vectors.c best.c pst.c
	$(CC) -O2 -o aquilon64 $^
	$(CC) -O2 -o aquilon32 $^ -m32

demo_fen: demo_fen.c chess.c utils.c vectors.c
	${CC} -Wall -Wpedantic -o $@ $^ -g

demo_perft: demo_perft.c chess.c utils.c vectors.c
	${CC} -Wall -Wpedantic -o $@ $^ -g -D CREATE_LOG

demo_perft_capablanca: demo_perft.c chess.c utils.c vectors.c
	${CC} -Wall -Wpedantic -o $@ $^ -D CAPABLANCA -g

demo_play: demo_play.c chess.c utils.c vectors.c
	${CC} -g -Wall -Wpedantic -o $@ $^

demo_image: demo_image.c image.c
	${CC} -Wall -Wpedantic -o $@ $^ -g -lcairo

demo_image2: demo_image2.c chess.c utils.c vectors.c image.c
	${CC} -Wall -Wpedantic -o $@ $^ -g -lcairo

demo_image3: demo_image3.c chess.c utils.c vectors.c image.c
	${CC} -Wall -Wpedantic -o $@ $^ -g -lcairo -lm

demo_window: demo_window.c chess.c utils.c vectors.c image.c
	${CC} -Wall -Wpedantic -o $@ $^ -g -lSDL -lcairo -lm

demo_window2: demo_window2.o cairosdl.o
	$(CC) -Wall -Wpedantic -o $@ $+ -g `pkg-config --libs sdl cairo` -lm

demo_window3: demo_window3.o
	$(CC) -Wall -Wpedantic -o $@ $+ -g -lSDL2 -lcairo -lm

demo_utils: demo_utils.c utils.c
	${CC} -Wall -Wpedantic -o $@ $^ -g

demo_pst: demo_pst.c utils.c pst.c
	LC_ALL=C ${CC} -Wall -Wpedantic -o $@ $^ -g

demo_fen_test: demo_fen
	# Classical chess start position
	./demo_fen "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1" &> demo_fen_test1.txt
	# Capablanca chess start position
	./demo_fen "rnabqkbcnr/pppppppppp/10/10/10/10/PPPPPPPPPP/RNABQKBCNR w KQkq - 0 1" &> demo_fen_test2.txt
	# X-FEN example (from https://sourceforge.net/p/scidvspc/patches/1/#cb90)
	./demo_fen "5rkr/8/8/8/8/8/8/R1R1K3 w Q - 0 1" &> demo_fen_test3.txt
	# X-FEN example (from https://sourceforge.net/p/scidvspc/patches/1/#2f33)
	./demo_fen "rn2k1r1/ppp1pp1p/3p2p1/5bn1/P7/2N2B2/1PPPPP2/2BNK1RR w Gkq - 4 11" &> demo_fen_test4.txt
	# S-FEN example (Fischer chess, start position 1)
	./demo_fen "bbqnnrkr/pppppppp/8/8/8/8/PPPPPPPP/BBQNNRKR w HFhf - 0 1" &> demo_fen_test5.txt

demo_fen_memory: demo_fen
	valgrind --leak-check=yes ./demo_fen 2> valgrind.err

preprocess: chess.c
	gcc -E chess.c > preprocessed.c
