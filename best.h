
#include "chess.h"

int piece_value(const char piece);
int evaluate_pawn_struct(const struct t_position position, const char player);
struct t_move best_move(struct t_position position);
