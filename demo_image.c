
#include "image.h"

int main(void)
{
  create_images();
  
  cairo_surface_t* chessboard_sfc = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 11 * 36, 9 * 36);
  cairo_t* chessboard_ctx = cairo_create(chessboard_sfc);
  
  cairo_select_font_face(chessboard_ctx, CHESSBOARD_FONT, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
  cairo_set_font_size(chessboard_ctx, 12);
  
  cairo_set_source_rgb(chessboard_ctx, BORDER_COLOR);
  cairo_paint(chessboard_ctx);
  
  draw_empty_chessboard(chessboard_ctx);
  
  draw_chesspiece(chessboard_ctx, 'A', 36 * 0 + 18, 36 * (7 - 7) + 18);
  draw_chesspiece(chessboard_ctx, 'a', 36 * 1 + 18, 36 * (7 - 7) + 18);
  draw_chesspiece(chessboard_ctx, 'B', 36 * 1 + 18, 36 * (7 - 6) + 18);
  
  cairo_destroy(chessboard_ctx);
  cairo_surface_write_to_png(chessboard_sfc, "demo_image.png");
  
  free_images();
  
  cairo_surface_destroy(chessboard_sfc);

  return 0;
}
