
#include "image.h"

cairo_surface_t* ba_sfc;
cairo_surface_t* bb_sfc;
cairo_surface_t* bc_sfc;
cairo_surface_t* bk_sfc;
cairo_surface_t* bn_sfc;
cairo_surface_t* bp_sfc;
cairo_surface_t* bq_sfc;
cairo_surface_t* br_sfc;
cairo_surface_t* wa_sfc;
cairo_surface_t* wb_sfc;
cairo_surface_t* wc_sfc;
cairo_surface_t* wk_sfc;
cairo_surface_t* wn_sfc;
cairo_surface_t* wp_sfc;
cairo_surface_t* wq_sfc;
cairo_surface_t* wr_sfc;

void create_images(void)
{
  ba_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "ba.png");
  bb_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "bb.png");
  bc_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "bc.png");
  bk_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "bk.png");
  bn_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "bn.png");
  bp_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "bp.png");
  bq_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "bq.png");
  br_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "br.png");
  wa_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wa.png");
  wb_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wb.png");
  wc_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wc.png");
  wk_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wk.png");
  wn_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wn.png");
  wp_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wp.png");
  wq_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wq.png");
  wr_sfc = cairo_image_surface_create_from_png(IMAGE_PATH "wr.png");
}

void free_images(void)
{
  cairo_surface_destroy(ba_sfc);
  cairo_surface_destroy(bb_sfc);
  cairo_surface_destroy(bc_sfc);
  cairo_surface_destroy(bk_sfc);
  cairo_surface_destroy(bn_sfc);
  cairo_surface_destroy(bp_sfc);
  cairo_surface_destroy(bq_sfc);
  cairo_surface_destroy(br_sfc);
  cairo_surface_destroy(wa_sfc);
  cairo_surface_destroy(wb_sfc);
  cairo_surface_destroy(wc_sfc);
  cairo_surface_destroy(wk_sfc);
  cairo_surface_destroy(wn_sfc);
  cairo_surface_destroy(wp_sfc);
  cairo_surface_destroy(wq_sfc);
  cairo_surface_destroy(wr_sfc);
}

void draw_empty_chessboard(cairo_t* ctx)
{
  int x, y;
  cairo_text_extents_t te;
  char character[2];
  character[1] = '\0';
  
  cairo_set_source_rgb(ctx, TEXT_COLOR);
  
  for (x = 0; x <= 9; x ++)
  {
    character[0] = x + 'A';
    cairo_text_extents (ctx, character, &te);
    cairo_move_to(ctx, 36 * x + 36 - te.width / 2 - te.x_bearing,          9 - te.height / 2 - te.y_bearing); cairo_show_text(ctx, character);
    cairo_move_to(ctx, 36 * x + 36 - te.width / 2 - te.x_bearing, 36 * 9 - 9 - te.height / 2 - te.y_bearing); cairo_show_text(ctx, character);
  }
  
  for (y = 0; y <= 7; y ++)
  {
    character[0] = y + '1';
    cairo_text_extents (ctx, character, &te);
    cairo_move_to(ctx,           9 - te.width / 2 - te.x_bearing, 36 * 8 - 36 * y - te.height / 2 - te.y_bearing); cairo_show_text(ctx, character);
    cairo_move_to(ctx, 36 * 11 - 9 - te.width / 2 - te.x_bearing, 36 * 8 - 36 * y - te.height / 2 - te.y_bearing); cairo_show_text(ctx, character);
  }
  
  for (x = 0; x <= 9; x ++)
  {
    for (y = 0; y <= 7; y ++)
    {
      if ((x + y) % 2 == 0)
      {
        cairo_set_source_rgb(ctx, LIGHT_SQUARE_COLOR);
      }
      else
      {
        cairo_set_source_rgb(ctx, DARK_SQUARE_COLOR);
      }
      cairo_rectangle(ctx, 36 * x + 18, 36 * y + 18, 36, 36),
      cairo_fill(ctx);
    }
  }
}

void draw_chesspiece(cairo_t* ctx, const char piece, const int x, const int y)
{
  cairo_surface_t* sfc;
  
  switch(piece) {
    case 'A' : sfc = wa_sfc; break;
    case 'B' : sfc = wb_sfc; break;
    case 'C' : sfc = wc_sfc; break;
    case 'K' : sfc = wk_sfc; break;
    case 'N' : sfc = wn_sfc; break;
    case 'P' : sfc = wp_sfc; break;
    case 'Q' : sfc = wq_sfc; break;
    case 'R' : sfc = wr_sfc; break;
    case 'a' : sfc = ba_sfc; break;
    case 'b' : sfc = bb_sfc; break;
    case 'c' : sfc = bc_sfc; break;
    case 'k' : sfc = bk_sfc; break;
    case 'n' : sfc = bn_sfc; break;
    case 'p' : sfc = bp_sfc; break;
    case 'q' : sfc = bq_sfc; break;
    case 'r' : sfc = br_sfc; break;
    default : return;
  }
  
  cairo_set_source_surface(ctx, sfc, x, y);
  cairo_paint(ctx);
}
